const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex)
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex)
    },
    hexToRGB: (hex) => {
        const rgb = []
        if(hex.length === 6){
            rgb.push(parseInt(hex.substring(0,2), 16).toString(10))
            rgb.push(parseInt(hex.substring(2,4), 16).toString(10))
            rgb.push(parseInt(hex.substring(4,6), 16).toString(10))
        } else if(hex.length === 3){
            rgb.push((parseInt(hex.substring(0,1), 16)*17).toString(10))
            rgb.push((parseInt(hex.substring(1,2), 16)*17).toString(10))
            rgb.push((parseInt(hex.substring(2,3), 16)*17).toString(10))
        } else {
            rgb.push("0", "0", "0")
        }

        return rgb
    }
}