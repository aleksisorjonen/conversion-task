const express = require('express')
const app = express()
const converter = require('./converter')
const port = 3000

// Endpoint localhost:3000/
app.get('/', (req, res) => res.send("Hello!"))

// Endpoint localhost:3000/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10)
    const green = parseInt(req.query.green, 10)
    const blue = parseInt(req.query.blue, 10)
    const hex = converter.rgbToHex(red, green, blue)
    res.send(hex)
})

// Endpoint localhost:3000/hex-to-rgb?hex=ffffff
app.get('/hex-to-rgb', (req, res) => {
    const color = converter.hexToRGB(req.query.hex)
    res.send(color[0] + ", " + color[1] + ", " + color[2])
})

if(process.env.NODE_ENV === 'test'){
    module.exports = app
} else {
    app.listen(port, () => console.log(`Server listening on localhost:${port}`))
}
