# Conversion task

1. Using Endpoint `localhost:3000/rgb-to-hex?red=255&green=0&blue=0`
2. Using Endpoint `localhost:3000/hex-to-rgb?hex=ffffff`

![alt text](postman1.PNG)
![alt text](postman2.PNG)