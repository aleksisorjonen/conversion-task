// TDD - Test Driven Developmnet - Unit testing

const expect = require('chai').expect
const converter = require('../src/converter')

describe("Color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0) // ff0000
            const greenHex = converter.rgbToHex(0,255,0) // 00ff00
            const blueHex = converter.rgbToHex(0,0,255) // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors correctly", () => {
            const red = converter.hexToRGB("ff0000")
            const green = converter.hexToRGB("00ff00")
            const blue = converter.hexToRGB("0000ff")

            expect(red[0]).to.equal("255")
            expect(red[1]).to.equal("0")
            expect(red[2]).to.equal("0")
            expect(green[0]).to.equal("0")
            expect(green[1]).to.equal("255")
            expect(green[2]).to.equal("0")
            expect(blue[0]).to.equal("0")
            expect(blue[1]).to.equal("0")
            expect(blue[2]).to.equal("255")
        })
        it("converts hex of 3 correctly", () => {
            const color1 = converter.hexToRGB("f0f")
            const color2 = converter.hexToRGB("6fa")

            expect(color1[0]).to.equal("255")
            expect(color1[1]).to.equal("0")
            expect(color1[2]).to.equal("255")

            expect(color2[0]).to.equal("102")
            expect(color2[1]).to.equal("255")
            expect(color2[2]).to.equal("170")
        })
    })
    
})